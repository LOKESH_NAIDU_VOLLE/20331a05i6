interface animal{
    void tiger();
    void rabbit();
}
class Main implements animal{
    public void tiger(){
        System.out.println("tiger roars");
    }
    public void rabbit(){
        System.out.println("rabbit runs");
    }
    
    public static void main(String[] args){
        Main obj=new Main();
        obj.tiger();
        obj.rabbit();
    }
}